package cymru.prv.api.geiriadur.welsh;

import cymru.prv.api.geiriadur.linguistics.welsh.Mutation;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

public class MutationTests {

    public static void assertMutations(JSONObject obj, String init, String soft, String nasal, String aspirate){
        Assert.assertEquals(obj.getString("init"), init);
        Assert.assertEquals(obj.getString("soft"), soft);
        Assert.assertEquals(obj.getString("nasal"), nasal);
        Assert.assertEquals(obj.getString("aspirate"), aspirate);
    }

    @Test
    public void testCeredigion(){
        JSONObject obj = Mutation.getMutations("ceredigion");
        assertMutations(obj,
                "ceredigion","geredigion","ngheredigion", "cheredigion");
    }

    @Test
    public void testLlaeth(){
        assertMutations(Mutation.getMutations("Llaeth"),
                "llaeth", "laeth","","");
    }

    @Test
    public void testGwyn(){
        assertMutations(Mutation.getMutations("gwyn"),
                "gwyn", "wyn","ngwyn","");
    }
}
