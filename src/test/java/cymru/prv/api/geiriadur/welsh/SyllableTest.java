package cymru.prv.api.geiriadur.welsh;

import cymru.prv.api.geiriadur.linguistics.welsh.Syllable;
import org.junit.Assert;
import org.junit.Test;

public class SyllableTest {

    @Test
    public void testSyllableCounter(){
        Assert.assertEquals(Syllable.countSyllables("ceredigion"), 5);
        Assert.assertEquals(Syllable.countSyllables("cymraeg"), 2);
        Assert.assertEquals(Syllable.countSyllables("cysysllteiriau"), 5);
    }

}
