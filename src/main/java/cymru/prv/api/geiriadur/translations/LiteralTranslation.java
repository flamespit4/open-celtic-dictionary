package cymru.prv.api.geiriadur.translations;

import org.json.JSONObject;

public class LiteralTranslation implements ITranslation {

    private final String translation;

    public LiteralTranslation(String translation) {
        this.translation = translation;
    }

    @Override
    public JSONObject getTranslation() {
        return new JSONObject().put("value", translation);
    }
}
