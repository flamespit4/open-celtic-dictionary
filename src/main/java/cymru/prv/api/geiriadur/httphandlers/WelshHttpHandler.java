package cymru.prv.api.geiriadur.httphandlers;

import com.sun.net.httpserver.HttpExchange;
import cymru.prv.api.geiriadur.APIServer;
import cymru.prv.api.geiriadur.linguistics.common.WordType;

import java.util.Map;

public class WelshHttpHandler extends AbstractHttpHandler {

    public WelshHttpHandler(APIServer server){
        super(server);
    }

    @Override
    protected void handleRequest(HttpExchange exchange, Map<String, String> getVariables) {

        boolean search = false;
        String typeStr = null;

        if(!getVariables.containsKey("gair")){
            sendBadRequest(exchange,"Missing search word field: gair");
            return;
        }
        if(getVariables.containsKey("math")){
            typeStr = getVariables.get("math");
        }
        if(getVariables.containsKey("chwilio")){
            try {
                search = Boolean.parseBoolean(getVariables.get("chwilio"));
            }
            catch (Exception e){
                sendBadRequest(exchange, "Failed to convert " + getVariables.get("chwilio") + " (argument chwilio) to a boolean");
                return;
            }
        }

        WordType type = null;
        if(typeStr != null){
            try {
                type = WordType.valueOf(getVariables.get("math"));
            }
            catch (Exception e){
                sendBadRequest(exchange, "Unknown word type " + getVariables.get("math"));
                return;
            }
        }

        dictionaryQuery(exchange, "cy", getVariables.get("gair"), search, type);
    }
}
