package cymru.prv.api.geiriadur.httphandlers;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import cymru.prv.api.geiriadur.APIServer;
import cymru.prv.api.geiriadur.AbstractDictionary;
import cymru.prv.api.geiriadur.linguistics.common.WordType;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * Abstract class that serves as a foundation for a dictionary API.
 *
 * @author Preben Vangberg
 * @since 2020.05.31
 */
public abstract class AbstractHttpHandler implements HttpHandler {

    protected APIServer server;

    /**
     * The default constructor for the AbstractHttpHandler.
     * The APIServer object is required for the HttpHandler to access the
     * DictionaryList.
     *
     * @param server The API server which the HttpHandler is associated with
     */
    protected AbstractHttpHandler(APIServer server){
        this.server = server;
    }

    /**
     * The abstract function that is called by handle after a HTTP request has
     * been received.
     *
     * @param exchange The HTTP request
     * @param getVariables The GET variables from the request URI.
     */
    protected abstract void handleRequest(HttpExchange exchange, Map<String, String> getVariables);

    /**
     * Gets the get variables from the request URI and puts them in a Map.
     *
     * @param exchange The HTTP request
     * @return the GET variables from the request URI as a map.
     */
    private Map<String, String> getGetVariables(HttpExchange exchange){
        Map<String, String> keyValueMap = new HashMap<>();
        String uriText = exchange.getRequestURI().getQuery();
        if(uriText == null)
            return keyValueMap;
        String[] keyValuePairs = uriText.split("&");


        for (String keyValuePair: keyValuePairs) {
            String[] keyValueArray = keyValuePair.split("=");
            String value = "";
            if(keyValueArray.length > 1)
                value = keyValueArray[1];
            keyValueMap.put(keyValueArray[0], value);
        }

        return keyValueMap;
    }

    /**
     * Helper function for sendValidResponse and sendBadRequest.
     *
     * Actually sends the JSONObject to the output stream of the HTTP request.
     *
     * @param exchange The HTTP request
     * @param response Thse JSON object to send
     */
    protected void sendJSONResponse(HttpExchange exchange, JSONObject response){
        byte[] responseBytes = response.toString().getBytes(StandardCharsets.UTF_8);
        try {
            exchange.getResponseHeaders().set("Content-Type", String.format("application/json; charset=%s", StandardCharsets.UTF_8));
            exchange.getResponseHeaders().set("Access-Control-Allow-Origin", "*");
            exchange.sendResponseHeaders(200, responseBytes.length);
            exchange.getResponseBody().write(responseBytes);
            exchange.getResponseBody().flush();
            exchange.getResponseBody().close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sends a JSON object with the success-flag set to true,
     * and with a JSONArray called results.
     *
     * @param exchange The active HttpExchange
     * @param data The array of results (words)
     */
    protected void sendValidResponse(HttpExchange exchange, JSONArray data){
        JSONObject obj = new JSONObject();
        obj.put("success", true);
        obj.put("results", data);
        sendJSONResponse(exchange, obj);
    }


    /**
     * Sends a JSON object with the success-flag set to false,
     * and with the given error message.
     *
     * @param exchange The active HttpExchange
     * @param error The error message
     */
    protected void sendBadRequest(HttpExchange exchange, String error){
        JSONObject response = new JSONObject();
        response.put("success", false);
        response.put("error", error);
        sendJSONResponse(exchange, response);
    }


    /**
     * The function that handle the HTTP request.
     *
     * Calls the abstract handleRequest function.
     *
     * @param httpExchange The HTTP request
     */
    @Override
    public void handle(HttpExchange httpExchange) {
        try {
            Map<String, String> getVariables = getGetVariables(httpExchange);
            handleRequest(httpExchange, getVariables);
        }catch (Exception e){
            e.printStackTrace();
            sendBadRequest(httpExchange, "Unknown server side error occured");
        }
    }

    /**
     * Executes the query on the DictionaryList in server.
     *
     * @param exchange The HTTP request
     * @param languageCode The language code of the dictionary
     * @param word The search term
     * @param search Search conjugated and other terms as well.
     * @param type Word type of the search term if applicable.
     */
    protected void dictionaryQuery(HttpExchange exchange, String languageCode, String word, boolean search, WordType type){
        AbstractDictionary dictionary = server.getDictionaries().getDictionaryByLangCode(languageCode.toLowerCase());
       if(dictionary == null){
           sendBadRequest(exchange, "Unknown language code '" + languageCode + "'");
       }
        else if(search){
            sendValidResponse(exchange, dictionary.searchForWord(word, 5));
        }
        else {
            if(type != null){
                sendValidResponse(exchange, dictionary.getWord(word, type));
            }
            else {
                sendValidResponse(exchange, dictionary.getWord(word));
            }
        }
    }
}
