package cymru.prv.api.geiriadur.linguistics.welsh.verb;

import cymru.prv.api.geiriadur.linguistics.common.Mutates;
import cymru.prv.api.geiriadur.linguistics.common.Verb;
import cymru.prv.api.geiriadur.linguistics.welsh.Mutation;
import cymru.prv.api.geiriadur.linguistics.welsh.verb.tenses.*;
import org.json.JSONObject;

public class WelshVerb extends Verb implements Mutates {

    public WelshVerb(JSONObject obj){
        super(obj);
    }

    @Override
    protected String getStem(String word){
        return word.replaceFirst("(a|i|o|u|eg|ed|an|yd|yll)$", "");
    }

    @Override
    protected void registerTenses(JSONObject obj) {
        addRequiredTense(obj, "preterite", PreteriteWelshVerbTense::new);
        addRequiredTense(obj, "future", FutureWelshVerbTense::new);
        addRequiredTense(obj, "imperative", ImperativeWelshVerbTense::new);
        addRequiredTense(obj, "conditional", ConditionalWelshVerbTense::new);
        addRequiredTense(obj, "pluperfect", PluperfectTenseWelsh::new);

        addOptionalTense(obj, "present", PresentWelshVerbTense::new);
        addOptionalTense(obj, "imperfect", ConditionalWelshVerbTense::new);

    }

    @Override
    public JSONObject getMutations() {
        return Mutation.getMutations(normalForm);
    }
}
