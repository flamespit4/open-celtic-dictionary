package cymru.prv.api.geiriadur.linguistics.welsh.verb.tenses;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

public class FutureWelshVerbTense extends WelshVerbTense {

    public FutureWelshVerbTense(String stem, JSONObject obj) {
        super(stem, obj);
    }

    @Override
    protected List<String> defaultSingFirst() {
        return Arrays.asList(apply("af"));
    }

    @Override
    protected List<String> defaultSingSecond() {
        return Arrays.asList(apply("i"));
    }

    @Override
    protected List<String> defaultSingThird() {
        return Arrays.asList(apply("iff"));
    }

    @Override
    protected List<String> defaultPlurFirst() {
        return Arrays.asList(apply("wn"));
    }

    @Override
    protected List<String> defaultPlurSecond() {
        return Arrays.asList(apply("wch"));
    }

    @Override
    protected List<String> defaultPlurThird() {
        return Arrays.asList(apply("an"));
    }
}
