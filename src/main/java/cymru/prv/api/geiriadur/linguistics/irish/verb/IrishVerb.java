package cymru.prv.api.geiriadur.linguistics.irish.verb;

import cymru.prv.api.geiriadur.linguistics.common.Verb;
import cymru.prv.api.geiriadur.linguistics.irish.verb.tenses.*;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.compile;

public class IrishVerb extends Verb {

    public static final String PRESENT_INDEPENDENT = "present_independent";
    public static final String PRESENT_DEPENDENT = "present_dependent";
    public static final String PRESENT_NEGATIVE = "present_negative";
    public static final String PRESENT_HABITUAL = "present_habitual";
    public static final String PAST_INDEPENDENT = "past_independent";
    public static final String PAST_DEPENDENT = "past_dependent";
    public static final String PAST_HABITUAL = "past_habitual";
    public static final String FUTURE_INDEPENDENT = "future_independent";
    public static final String FUTURE_DEPENDENT = "future_dependent";
    public static final String CONDITIONAL_INDEPENDENT = "conditional_independent";
    public static final String CONDITIONAL_DEPENDENT = "conditional_dependent";

    /**
     * Reads data from the JSON object
     * and fills out the necessary fields.
     *
     * @param obj a JSON object containing at least
     *            the field "normalForm". The field
     *            "notes" is optional
     */
    public IrishVerb(JSONObject obj) {
        super(obj);
    }


    @Override
    protected String getStem(String normalForm) {
        if(countSyllables(normalForm) == 1)
            return normalForm;
        else {
            return replaceLast(normalForm);
                    //.replaceFirst("(igh|aigh|ail|is)$", "");
        }
    }

    @Override
    protected void registerTenses(JSONObject obj) {
        if(normalForm.equals(stem)){
            addRequiredTense(obj, PRESENT, FirstPresentIrishVerbTense::new);
            addRequiredTense(obj,IMPERFECT, FirstImperfectIrishVerbTense::new);
            addRequiredTense(obj, PRETERITE, FirstPreteriteIrishVerbTense::new);
            addRequiredTense(obj, FUTURE, FirstFutureIrishVerbTense::new);
            addRequiredTense(obj, CONDITIONAL, FirstConditionalIrishVerbTense::new);
            addRequiredTense(obj, IMPERATIVE, FirstImperativeIrishVerbTense::new);

            addOptionalTense(obj, PRESENT_INDEPENDENT, FirstPresentIrishVerbTense::new);
            addOptionalTense(obj, PRESENT_DEPENDENT, FirstPresentIrishVerbTense::new);
            addOptionalTense(obj, PRESENT_NEGATIVE, FirstPresentIrishVerbTense::new);
            addOptionalTense(obj, PRESENT_HABITUAL, FirstPresentIrishVerbTense::new);

            addOptionalTense(obj, PAST_INDEPENDENT, FirstPreteriteIrishVerbTense::new);
            addOptionalTense(obj, PAST_DEPENDENT, FirstPreteriteIrishVerbTense::new);
            addOptionalTense(obj, PAST_HABITUAL, FirstImperfectIrishVerbTense::new);

            addOptionalTense(obj, FUTURE_INDEPENDENT, FirstFutureIrishVerbTense::new);
            addOptionalTense(obj, FUTURE_DEPENDENT, FirstFutureIrishVerbTense::new);

            addOptionalTense(obj, CONDITIONAL_INDEPENDENT, FirstConditionalIrishVerbTense::new);
            addOptionalTense(obj, CONDITIONAL_DEPENDENT, FirstConditionalIrishVerbTense::new);
        }
        else {
            addRequiredTense(obj, "present", SecondPresentIrishVerbTense::new);
            addRequiredTense(obj, "imperfect", SecondImperfectVerbTense::new);
            addTense("preterite", new SecondPreteriteIrishVerbTense(normalForm, stem,
                    obj.has("preterite") ? obj.getJSONObject("preterite") : new JSONObject("{}")));
            addRequiredTense(obj, "future", SecondFutureIrishVerbTense::new);
            addRequiredTense(obj, "conditional", SecondConditionalIrishVerbTense::new);
            addTense("imperative", new SecondImperativeIrishVerbTense(normalForm, stem,
                    obj.has("imperative") ? obj.getJSONObject("imperative") : new JSONObject()));
            if(obj.has(PAST_INDEPENDENT))
                addTense(PAST_INDEPENDENT, new SecondPreteriteIrishVerbTense(normalForm, stem, obj.getJSONObject(PAST_INDEPENDENT)));
            if(obj.has(PAST_DEPENDENT))
                addTense(PAST_DEPENDENT, new SecondPreteriteIrishVerbTense(normalForm, stem, obj.getJSONObject(PAST_DEPENDENT)));
        }
    }

    private static final String  regexVowels = "(aío|aoi|aoú|ia(i|)|ua(i|)|eái|ea(i|)|eo(i|)|uío|uói|iái|iói|iúi|uái|oío|" +
            "ae(i|)|ái|ai|ao|aí|éa|eá|ei|éi|ío|" +
            "uó|oi|iá|ió|iú|io|uí|úi|iu|uá|ói|oí|ui|á|a|e|é|i|í|o|ó|u|ú)";

    private long countSyllables(String normalForm){
        return compile(regexVowels).matcher(normalForm).results().count();
    }

    public static String replaceLast(String text) {
        MatchResult result = null;
        for (Iterator<MatchResult> it = Pattern.compile(regexVowels).matcher(text).results().iterator(); it.hasNext(); ) {
            result = it.next();
        }
        if(result == null)
            return text;

        String base = text.substring(0, result.start());
        String end = text.substring(result.end());
        if(end.matches("(dh|gh)"))
            return base;
        return base + end;
    }

}
