package cymru.prv.api.geiriadur.linguistics.irish;

import java.util.Arrays;
import java.util.Collections;

public class IrishLenition {

    public static String preformLenition(String word){
        word = word
                .replaceFirst("^p", "ph")
                .replaceFirst("^t", "th")
                .replaceFirst("^c", "ch")
                .replaceFirst("^b", "bh")
                .replaceFirst("^d", "dh")
                .replaceFirst("^g", "gh")
                .replaceFirst("^m", "mh")
                .replaceFirst("^s", "sh")
                .replaceFirst("^f", "fh");
        if(word.matches("^(a|e|u|i|o|fh).*"))
            word = "d'" + word;
        return word;
    }

    public static boolean isBroad(String word){
        int lastBroad = Collections.max(Arrays.asList(
                word.lastIndexOf("a"),
                word.lastIndexOf("á"),
                word.lastIndexOf("u"),
                word.lastIndexOf("ú"),
                word.lastIndexOf("o"),
                word.lastIndexOf("ó")
        ));
        int lastSlender = Collections.max(Arrays.asList(
                word.lastIndexOf("e"),
                word.lastIndexOf("é"),
                word.lastIndexOf("i"),
                word.lastIndexOf("í")
        ));
        if(lastBroad > lastSlender)
            return true;
        return false;
    }

}
