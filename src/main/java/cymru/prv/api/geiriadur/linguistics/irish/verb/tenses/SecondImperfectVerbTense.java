package cymru.prv.api.geiriadur.linguistics.irish.verb.tenses;

import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

public class SecondImperfectVerbTense extends LenitedIrishVerbTense {

    public SecondImperfectVerbTense(String stem, JSONObject obj) {
        super(stem, obj);
    }

    @Override
    protected List<String> defaultAnalytic() {
        return Collections.singletonList(applyBroadOrSlender("aíodh","íodh"));
    }

    @Override
    protected List<String> defaultSingFirst() {
        return Collections.singletonList(applyBroadOrSlender("aínn", "ínn"));
    }

    @Override
    protected List<String> defaultSingSecond() {
        return Collections.singletonList(applyBroadOrSlender("aíteá", "íteá"));
    }

    @Override
    protected List<String> defaultSingThird() {
        return defaultAnalytic();
    }

    @Override
    protected List<String> defaultPlurFirst() {
        return Collections.singletonList(applyBroadOrSlender("aímis", "ímis"));
    }

    @Override
    protected List<String> defaultPlurSecond() {
        return defaultAnalytic();
    }

    @Override
    protected List<String> defaultPlurThird() {
        return Collections.singletonList(applyBroadOrSlender("aídís", "ídís"));
    }

    @Override
    protected List<String> defaultImpersonal() {
        return Collections.singletonList(applyBroadOrSlender("aítí", "ítí"));
    }
}
