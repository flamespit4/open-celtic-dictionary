package cymru.prv.api.geiriadur.linguistics.breton.verb.tenses;

import cymru.prv.api.geiriadur.linguistics.common.VerbTense;
import org.json.JSONObject;

public abstract class BretonVerbTense extends VerbTense {

    public BretonVerbTense(String stem, JSONObject obj) {
        super(stem, obj);
    }

    @Override
    protected String apply(String suffix) {
        return stem + suffix;
    }

}
