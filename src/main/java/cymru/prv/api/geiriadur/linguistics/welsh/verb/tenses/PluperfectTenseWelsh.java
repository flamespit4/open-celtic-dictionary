package cymru.prv.api.geiriadur.linguistics.welsh.verb.tenses;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

public class PluperfectTenseWelsh extends WelshVerbTense {

    public PluperfectTenseWelsh(String stem, JSONObject obj) {
        super(stem, obj);
    }

    @Override
    protected List<String> defaultSingFirst() {
        return Arrays.asList(apply("swn"));
    }

    @Override
    protected List<String> defaultSingSecond() {
        return Arrays.asList(apply("sit"));
    }

    @Override
    protected List<String> defaultSingThird() {
        return Arrays.asList(apply("sai"));
    }

    @Override
    protected List<String> defaultPlurFirst() {
        return Arrays.asList(apply("sem"));
    }

    @Override
    protected List<String> defaultPlurSecond() {
        return Arrays.asList(apply("sech"));
    }

    @Override
    protected List<String> defaultPlurThird() {
        return Arrays.asList(apply("sent"));
    }
}
