package cymru.prv.api.geiriadur.linguistics.common;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

public class Noun extends Word {

    private final List<String> plurals = new LinkedList<>();
    private final NounGender gender;

    private final boolean hasIsCountable;
    private boolean isCountable;

    public Noun(JSONObject obj) {
        super(obj, WordType.noun);
        if (obj.has("plural")) {
            JSONArray pluralsArray = obj.getJSONArray("plural");
            for (int i = 0; i < pluralsArray.length(); i++)
                plurals.add(pluralsArray.getString(i));
        }

        hasIsCountable = obj.has("isCountable");
        if (hasIsCountable)
            isCountable = obj.optBoolean("isCountable");
        gender = obj.optEnum(NounGender.class, "gender", null);
    }

    @Override
    public JSONObject toJson() {
        JSONObject obj = createBaseJson();
        if (plurals.size() > 0)
            obj.put("plural", plurals);
        if (hasIsCountable)
            obj.put("isCountable", isCountable);
        if (gender != null)
            obj.put("gender", gender);
        return obj;
    }

    @Override
    public long getNumberOfVersions() {
        return super.getNumberOfVersions() + plurals.size();
    }

    @Override
    public boolean matchesSearch(String search) {
        return super.matchesSearch(search)
                || plurals.stream().anyMatch(s -> s.matches(search));
    }
}
