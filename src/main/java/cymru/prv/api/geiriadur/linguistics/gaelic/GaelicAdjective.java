package cymru.prv.api.geiriadur.linguistics.gaelic;

import cymru.prv.api.geiriadur.linguistics.common.Adjective;
import org.json.JSONObject;

public class GaelicAdjective extends Adjective {

	private String stem;

	public GaelicAdjective(JSONObject obj) {
		super(obj);
		stem = normalForm;
		stem = obj.optString("stem", stem);
	}

	protected String getEquative() {
		return null;
	}

	@Override
	protected String getComparative() {
		return "nas " + attenuate(stem) + "e";
	}

	@Override
	protected String getSuperlative() {
		return "as " + attenuate(stem) + "e";
	}

	protected static String attenuate(String stem) {
		int lastVowel = -1;

		for (int i = 0; i < stem.length(); i++) {
			if (stem.charAt(i) == 'a' || stem.charAt(i) == 'e' || stem.charAt(i) == 'i' || stem.charAt(i) == 'o'
					|| stem.charAt(i) == 'u' || stem.charAt(i) == 'à' || stem.charAt(i) == 'è' || stem.charAt(i) == 'ì'
					|| stem.charAt(i) == 'ò' || stem.charAt(i) == 'ù') {
				lastVowel = i;
			}
		}

		String att = "";

		if (stem.charAt(lastVowel) == 'e' && stem.charAt(lastVowel) == 'i' && stem.charAt(lastVowel) == 'è'
				&& stem.charAt(lastVowel) == 'ì') {
			att = stem;
		} else {
			if (stem.charAt(lastVowel - 1) == 'e' && stem.charAt(lastVowel) == 'a') {
				for (int i = 0; i < stem.length(); i++) {
					if (i == lastVowel) {
						// do nothing
					} else if (i == lastVowel - 1) {
						att += "i";
					} else {
						att += stem.charAt(i);
					}
				}
			} else {
				for (int i = 0; i < stem.length(); i++) {
					att += stem.charAt(i);
					if (i == lastVowel) {
						att += "i";
					}
				}
			}
		}

		return att;
	}
}
