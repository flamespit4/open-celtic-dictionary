package cymru.prv.api.geiriadur.linguistics.welsh;

import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Mutation contains several static functions relating to
 * mutations of words in the Welsh language.
 *
 * An instance of the class represent a single row in
 * the mutation table.
 *
 * @author Preben Vangberg
 * @since 2020-04-21
 */
public class Mutation {

    public static final String SOFT = "soft";
    public static final String NASAL = "nasal";
    public static final String ASPIRATE = "aspirate";

    private String init;
    private String soft;
    private String nasal;
    private String aspirate;

    private Mutation(String init, String soft, String nasal, String aspirate) {
        this.init = init;
        this.soft = soft;
        this.nasal = nasal;
        this.aspirate = aspirate;
    }

    private static final Map<String, Mutation> mutationMap = createMutationTable();

    private static final Mutation defaultMutation = new Mutation(null, null, null, null);

    private static Map<String, Mutation> createMutationTable(){
        return Map.of(
                "p", new Mutation("p", "b" , "mh", "ph" ),
                "t", new Mutation("t", "d" , "nh", "th" ),
                "c", new Mutation("c", "g" , "ngh", "ch" ),
                "b", new Mutation("b", "f", "m", null),
                "d", new Mutation("d", "dd", "n", null),
                "g", new Mutation("g", "-", "ng", null),
                "m", new Mutation("m", "f", null, null),
                "ll", new Mutation("ll", "l", null, null),
                "rh", new Mutation("rh", "r", null, null));
    }

    /**
     * Creates a new mutation table and returns it.
     *
     * @return a map of mutations.
     */
    public static Map<String, Mutation> getMutationTable(){
        return createMutationTable();
    }

    /**
     * Creates a JSONObject containing the different mutations
     * of the given word. These are theoretical and may not
     * appear in the real word.
     *
     * @param word The word to be mutated.
     * @return the mutations of the word as a JSONObject.
     */
    public static JSONObject getMutations(String word){
        word = word.toLowerCase();
        JSONObject obj = new JSONObject();
        Mutation mutation = getMutation(word);
        obj.put("init", word);
        obj.put(SOFT, mutation.getTregladMeddal(word));
        obj.put(NASAL, mutation.getTregladTrywynol(word));
        obj.put(ASPIRATE, mutation.getTregladLlaes(word));
        return obj;
    }

    public static List<String> getMutationsAsList(String word){
        word = word.toLowerCase();
        Mutation mut = getMutation(word);

        List<String> muts = new LinkedList<>();
        muts.add(mut.getTregladMeddal(word));
        muts.add(mut.getTregladTrywynol(word));
        muts.add(mut.getTregladLlaes(word));

        return muts;
    }

    public static String tregladMeddal(String word){
        String meddal = getMutation(word).getTregladMeddal(word);
        if(meddal.isEmpty())
            return word;
        return meddal;
    }

    private static Mutation getMutation(String word){
        if(word.startsWith("rh"))
            return mutationMap.get("rh");
        if(word.startsWith("ll"))
            return mutationMap.get("ll");
         return mutationMap.getOrDefault(word.substring(0,1), defaultMutation);
    }

    private String getTreglad(String word, String replacement){
        if(replacement == null)
            return "";
        if(replacement.equals("-"))
            return word.substring(1);
        return word.replaceFirst("^" + init, replacement);
    }

    private String getTregladMeddal(String word){
        return getTreglad(word, soft);
    }

    private String getTregladTrywynol(String word){
        return getTreglad(word, nasal);
    }

    private String getTregladLlaes(String word){
        return getTreglad(word, aspirate);
    }

}
