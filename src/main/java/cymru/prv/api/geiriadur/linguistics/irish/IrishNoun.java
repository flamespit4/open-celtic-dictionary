package cymru.prv.api.geiriadur.linguistics.irish;

import cymru.prv.api.geiriadur.linguistics.common.Noun;
import cymru.prv.api.geiriadur.linguistics.common.NounCase;
import org.json.JSONObject;

import java.util.*;

public class IrishNoun extends Noun {

    private static final List<String> NOUNCASES = Arrays.asList(
      "vocative",
      "genitive",
      "dative"
    );

    private Map<String, NounCase> cases = new HashMap<>();

    public IrishNoun(JSONObject obj) {
        super(obj);
        for(String caseName : NOUNCASES)
            if(obj.has(caseName))
                cases.put(caseName, new NounCase(obj.getJSONObject(caseName)));
    }

    @Override
    public JSONObject toJson() {
        JSONObject obj = super.toJson();
        for(String caseName : cases.keySet())
            obj.put(caseName, cases.get(caseName).toJson());
        return obj;
    }
}
