package cymru.prv.api.geiriadur.linguistics.irish.verb.tenses;

import cymru.prv.api.geiriadur.linguistics.irish.IrishLenition;
import org.json.JSONObject;

public abstract class LenitedIrishVerbTense extends IrishVerbTense {

    protected final boolean hasLenition;

    public LenitedIrishVerbTense(String stem, JSONObject obj, boolean defaultHasLenition){
        super(stem, obj);
        hasLenition = obj.optBoolean("hasLenition", defaultHasLenition);
    }

    public LenitedIrishVerbTense(String stem, JSONObject obj) {
        this(stem, obj, true);
    }

    protected String applyBroadOrSlenderWithoutLenition(String broad, String slender){
        return super.applyBroadOrSlender(broad, slender);
    }

    @Override
    protected String applyBroadOrSlender(String broad, String slender) {
        if(hasLenition)
            return IrishLenition.preformLenition(super.applyBroadOrSlender(broad, slender));
        else
            return super.applyBroadOrSlender(broad, slender);
    }

}
