package cymru.prv.api.geiriadur.linguistics.welsh;

import cymru.prv.api.geiriadur.AbstractDictionary;
import cymru.prv.api.geiriadur.DictionaryList;
import cymru.prv.api.geiriadur.linguistics.common.InflectedPreposition;
import cymru.prv.api.geiriadur.linguistics.common.Word;
import cymru.prv.api.geiriadur.linguistics.common.WordType;
import cymru.prv.api.geiriadur.linguistics.welsh.verb.WelshVerb;
import org.json.JSONObject;

import java.util.Map;
import java.util.function.Function;

/**
 * The main dictionary class. This represent a normal dictionary.
 *
 * The class contains a map of dictionaries containing exceptions
 * from the standard conjunction and stem rules.
 *
 * It expects to find the different JSON exception files in the relative folder res/
 *
 * @author Preben Vangberg
 * @since 2020-04-21
 */
public class WelshDictionary extends AbstractDictionary {

    private static final String languageName = "Welsh";
    private static final String languageCode = "cy";
    private static final Map<WordType, Function<JSONObject, Word>> wordTypeFromJsonMap = Map.of(
            WordType.verb, WelshVerb::new,
            WordType.adjective, WelshAdjective::new,
            WordType.noun, WelshNoun::new,
            WordType.preposition, InflectedPreposition::new
    );

    public WelshDictionary(){
        super(null, languageName, languageCode, wordTypeFromJsonMap);
    }

    public WelshDictionary(DictionaryList list) {
        super(list, languageName, languageCode, wordTypeFromJsonMap);
    }
}
