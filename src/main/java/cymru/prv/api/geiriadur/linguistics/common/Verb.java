package cymru.prv.api.geiriadur.linguistics.common;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

public abstract class Verb extends Word {

    public static final String PRESENT = "present";
    public static final String PRETERITE = "preterite";
    public static final String IMPERATIVE = "imperative";
    public static final String IMPERFECT = "imperfect";
    public static final String FUTURE = "future";
    public static final String CONDITIONAL = "conditional";

    protected String stem;
    protected final boolean conjugates;

    protected abstract String getStem(String normalForm);
    protected abstract void registerTenses(JSONObject obj);

    private Map<String, VerbTense> tenses = new HashMap<>();

    /**
     * Reads data from the JSON object
     * and fills out the necessary fields.
     *
     * @param obj  a JSON object containing at least
     *             the field "normalForm". The field
     *             "notes" is optional
     */
    public Verb(JSONObject obj) {
        super(obj, WordType.verb);
        conjugates = obj.optBoolean("conjugates", true);
        stem = obj.optString("stem", getStem(normalForm));
        if(conjugates)
            registerTenses(obj);
    }

    @Override
    public JSONObject toJson() {
        JSONObject obj = createBaseJson();
        if(!conjugates)
            return obj;

        obj.put("stem", stem);

        for (String tense : tenses.keySet())
            obj.put(tense, tenses.get(tense).toJson());

        return obj;
    }

    protected void addTense(String name, VerbTense tense){
        tenses.put(name, tense);
    }

    protected void addOptionalTense(JSONObject obj, String tense,  BiFunction<String, JSONObject, VerbTense> constructor){
        if(obj.has(tense)){
            VerbTense tenseObj = constructor.apply(stem, obj.getJSONObject(tense));
            if(tenseObj.hasTense())
                tenses.put(tense, tenseObj);
        }
    }

    protected void addRequiredTense(JSONObject obj, String tense, BiFunction<String, JSONObject, VerbTense> constructor){
        if(obj.has(tense)){
            VerbTense tenseObj = constructor.apply(stem, obj.getJSONObject(tense));
            if(tenseObj.hasTense())
                tenses.put(tense, tenseObj);
        }
        else {
            tenses.put(tense, constructor.apply(stem, new JSONObject("{}")));
        }
    }


    @Override
    public long getNumberOfVersions() {
        long number = 0;
        for(VerbTense tense : tenses.values())
            number += tense.numberOfConjugations();
        return super.getNumberOfVersions() + number;
    }

    @Override
    public boolean matchesSearch(String search) {
        if(super.matchesSearch(search))
            return true;

        for(VerbTense tense : tenses.values())
            if(tense.hasMatch(search))
                return true;

        return false;
    }

}
