package cymru.prv.api.geiriadur.linguistics.breton.verb.tenses;

import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

public class CondtitionalImperfectBretonVerbTense extends BretonVerbTense {

    public CondtitionalImperfectBretonVerbTense(String stem, JSONObject obj) {
        super(stem, obj);
    }

    @Override
    protected List<String> defaultSingFirst() {
        return Collections.singletonList(apply("jen"));
    }

    @Override
    protected List<String> defaultSingSecond() {
        return Collections.singletonList(apply("jes"));
    }

    @Override
    protected List<String> defaultSingThird() {
        return Collections.singletonList(apply("je"));
    }

    @Override
    protected List<String> defaultPlurFirst() {
        return Collections.singletonList(apply("jemp"));
    }

    @Override
    protected List<String> defaultPlurSecond() {
        return Collections.singletonList(apply("jec'h"));
    }

    @Override
    protected List<String> defaultPlurThird() {
        return Collections.singletonList(apply("jent"));
    }

    @Override
    protected List<String> defaultImpersonal() {
        return Collections.singletonList(apply("jed"));
    }
}
