package cymru.prv.api.geiriadur.linguistics.common;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

public class NounCase {

    private List<String> singluars;
    private List<String> plurals;

    public NounCase(JSONObject caseObj){
        singluars = getStringList(caseObj, "singular");
        plurals = getStringList(caseObj, "plural");
    }

    public JSONObject toJson(){
        return new JSONObject()
            .put("singular", singluars)
            .put("plural", plurals);
    }

    protected List<String> getStringList(JSONObject obj, String key){
        List<String> list = new LinkedList<>();
        if(obj.has(key)) {
            Object conjugation = obj.get(key);
            if(conjugation instanceof JSONArray)
                for(int i = 0; i < ((JSONArray) conjugation).length(); ++i)
                    list.add(((JSONArray) conjugation).getString(i));
            else
                list.add((String)conjugation);
        }
        return list;
    }

}
