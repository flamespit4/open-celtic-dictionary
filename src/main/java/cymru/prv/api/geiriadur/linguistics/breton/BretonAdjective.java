package cymru.prv.api.geiriadur.linguistics.breton;

import cymru.prv.api.geiriadur.linguistics.common.Adjective;
import cymru.prv.api.geiriadur.linguistics.common.Mutates;
import org.json.JSONObject;

public class BretonAdjective extends Adjective implements Mutates {

    private final String stem;

    private final String exclamative;

    public BretonAdjective(JSONObject obj) {
        super(obj);
        stem = obj.optString("stem", getStem(normalForm));
        exclamative = obj.optString("exclamative", null);
    }

    protected String getStem(String normalForm) {
        // Create stem
        return normalForm.replaceFirst("(a|i|o|u|an|añ|at|ein|ed|eg|et|in|iñ|yd)$", "")
                // provecation
                .replaceFirst("b$", "p")
                .replaceFirst("z$", "s");
    }

    @Override
    protected String getEquative() {
        return equative != null ? equative : "ken " + normalForm;
    }

    @Override
    protected String getComparative() {
        return comparative != null ? comparative : stem + "oc'h";
    }

    @Override
    protected String getSuperlative() {
        return superlative != null ? superlative : stem + "añ";
    }

    private String getExlamative(){
        return exclamative != null ? exclamative : stem + "at";
    }

    @Override
    public JSONObject getMutations() {
        return BretonMutation.getMutations(normalForm);
    }

    @Override
    public JSONObject toJson() {
        return super.toJson().put("exclamative", getExlamative());
    }

    @Override
    public long getNumberOfVersions() {
        return super.getNumberOfVersions() + (comparable ? 1 : 0);
    }
}
