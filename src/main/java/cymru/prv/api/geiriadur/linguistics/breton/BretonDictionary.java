package cymru.prv.api.geiriadur.linguistics.breton;

import cymru.prv.api.geiriadur.AbstractDictionary;
import cymru.prv.api.geiriadur.DictionaryList;
import cymru.prv.api.geiriadur.linguistics.common.InflectedPreposition;
import cymru.prv.api.geiriadur.linguistics.common.Word;
import cymru.prv.api.geiriadur.linguistics.common.WordType;
import cymru.prv.api.geiriadur.linguistics.breton.verb.BretonVerb;
import org.json.JSONObject;

import java.util.Map;
import java.util.function.Function;

public class BretonDictionary extends AbstractDictionary {

    private static final Map<WordType, Function<JSONObject, Word>> wordTypeToJsonMap = Map.of(
            WordType.verb, BretonVerb::new,
            WordType.noun, BretonNoun::new,
            WordType.adjective, BretonAdjective::new,
            WordType.preposition, InflectedPreposition::new
    );

    public BretonDictionary(DictionaryList dictionaryList) {
        super(dictionaryList, "Breton", "br", wordTypeToJsonMap);
    }
}
