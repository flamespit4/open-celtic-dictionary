package cymru.prv.api.geiriadur.linguistics.breton.verb.tenses;

import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

public class ConditionalPresentBretonVerbTense extends BretonVerbTense {

    public ConditionalPresentBretonVerbTense(String stem, JSONObject obj) {
        super(stem, obj);
    }

    @Override
    protected List<String> defaultSingFirst() {
        return Collections.singletonList(apply("fen"));
    }

    @Override
    protected List<String> defaultSingSecond() {
        return Collections.singletonList(apply("fes"));
    }

    @Override
    protected List<String> defaultSingThird() {
        return Collections.singletonList(apply("fe"));
    }

    @Override
    protected List<String> defaultPlurFirst() {
        return Collections.singletonList(apply("femp"));
    }

    @Override
    protected List<String> defaultPlurSecond() {
        return Collections.singletonList(apply("fec'h"));
    }

    @Override
    protected List<String> defaultPlurThird() {
        return Collections.singletonList(apply("fent"));
    }

    @Override
    protected List<String> defaultImpersonal() {
        return Collections.singletonList(apply("fed"));
    }
}
