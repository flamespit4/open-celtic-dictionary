package cymru.prv.api.geiriadur.linguistics.irish.verb.tenses;

import cymru.prv.api.geiriadur.linguistics.irish.IrishLenition;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

public class SpecialIrishVerbTense extends LenitedIrishVerbTense {

    public SpecialIrishVerbTense(String stem, JSONObject obj) {
        super(stem, obj, false);
    }

    @Override
    protected List<String> defaultAnalytic() {
        if(defaults != null)
            return getDefaultsOrAnalytic();
        if(hasLenition)
            return Collections.singletonList(IrishLenition.preformLenition(stem));
        return Collections.singletonList(stem);
    }

    @Override
    protected List<String> defaultSingFirst() {
        return getDefaultsOrAnalytic();
    }

    @Override
    protected List<String> defaultSingSecond() {
        return getDefaultsOrAnalytic();
    }

    @Override
    protected List<String> defaultSingThird() {
        return getDefaultsOrAnalytic();
    }

    @Override
    protected List<String> defaultPlurFirst() {
        return getDefaultsOrAnalytic();
    }

    @Override
    protected List<String> defaultPlurSecond() {
        return getDefaultsOrAnalytic();
    }

    @Override
    protected List<String> defaultPlurThird() {
        return getDefaultsOrAnalytic();
    }

    @Override
    protected List<String> defaultImpersonal() {
        return getDefaultsOrAnalytic();
    }
}
