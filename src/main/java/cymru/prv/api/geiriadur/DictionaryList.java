package cymru.prv.api.geiriadur;

import cymru.prv.api.geiriadur.linguistics.breton.BretonDictionary;
import cymru.prv.api.geiriadur.linguistics.common.Word;
import cymru.prv.api.geiriadur.linguistics.common.WordType;
import cymru.prv.api.geiriadur.linguistics.gaelic.GaelicDictionary;
import cymru.prv.api.geiriadur.linguistics.irish.IrishDictionary;
import cymru.prv.api.geiriadur.linguistics.welsh.WelshDictionary;
import cymru.prv.api.geiriadur.translations.TranslationUnit;
import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.function.Function;


/**
 * Represents a list of dictionaries which are identified by a language code.
 * This is works as a bridge between the dictionaries, which means that it is responsible for
 * loading inter-dictionary translations.a
 *
 * @author Preben Vangberg
 * @since 2020.05.31
 */
public class DictionaryList {

    /**
     * A map with Words and the ID's of these Word objects.
     * @see Word
     */
    private Map<Long, Word> wordIndexMap = new HashMap<>();

    /**
     * List of dictionaries. The key is the language code of the given dictionary
     * which is fetched from the Dictionary object itself.
     *
     * @see AbstractDictionary
     */
    private Map<String, AbstractDictionary> dictionaries = new HashMap<>();

    /**
     * A list of all the dictionary constructors.
     * DictionaryList will initiate all of these upon construction.
     */
    private List<Function<DictionaryList, AbstractDictionary>> dictionaryConstructors = Arrays.asList(
            WelshDictionary::new,
            BretonDictionary::new,
            IrishDictionary::new,
            GaelicDictionary::new
    );

    /**
     * Default constructor for Dictionarylist.
     *
     * This will construct all the dictionaries is dictionaryConstructors.
     * Afterwards it will load all the translations in res/trans.json and
     * add these to the required Word objects.
     *
     * @throws Exception If any error occur during construction of the object.
     */
    public DictionaryList() throws Exception {
        // Create dictionaries
        for (Function<DictionaryList, AbstractDictionary> constructor : dictionaryConstructors){
            AbstractDictionary dictionary = constructor.apply(this);
            dictionaries.put(dictionary.getLanguageCode(), dictionary);
        }

        // Do post creation linking
        for(Word word : wordIndexMap.values())
            word.postLinker(this);

        // Get translations
        JSONArray array = new JSONArray(Files.readString(Path.of("res/trans.json")));
        for (int i = 0; i < array.length(); i++)
            new TranslationUnit(this, array.getJSONObject(i));
    }


    /**
     * Returns the word on a given index or null
     * @param index the word index you want.
     * @return Returns the word if the index exist, otherwise null.
     */
    public Word getWordAtIndex(long index) {
        return wordIndexMap.getOrDefault(index, null);
    }


    /**
     * Puts the given word in the ID / Word map.
     * The ID is fetched from the word object.
     *
     * @throws IllegalArgumentException If a duplicate word entry id already exist.
     * @param word The word you want to add register the ID of.
     */
    public void putWord(Word word) {
        if(wordIndexMap.containsKey(word.getId())) {
            throw new IllegalArgumentException("Duplicate word entry id " +
                    word.getId() + ". " + word.getNormalForm() + ", " +
                    wordIndexMap.get(word.getId()).getNormalForm());
        }
        wordIndexMap.put(word.getId(), word);
    }


    /**
     * Returns a the dictionary associated with a given language code.
     * @param langCode The language code of the dictionary
     * @return The dictionary if it exist. Otherwise null.
     */
    public AbstractDictionary getDictionaryByLangCode(String langCode){
        return dictionaries.get(langCode);
    }


    /**
     * Fetches the dictionary with the given language-code and gets the word from the
     * dictionary with the specified type.
     *
     * @see AbstractDictionary
     * @param languageCode The language code of the dictionary
     * @param word The word in the dictionary you want
     * @param type The type of the word you want
     * @return A JSONArray with the words or generated result.
     */
    public JSONArray getWord(String languageCode, String word, WordType type){
        return dictionaries.get(languageCode).getWord(word, type);
    }


    /**
     * Fetches the dictionary with the given language-code and gets the word from the
     * dictionary independently of the type.
     *
     * @see AbstractDictionary
     * @param languageCode the language code of the dictionary
     * @param word the word in the dictionary you want
     * @return All the words in the dictionary that matches the word
     */
    public JSONArray getWord(String languageCode, String word){
        return dictionaries.get(languageCode).getWord(word);
    }


    /**
     * Fetches the dictionary with the given language-code, and searches for match.
     *
     * @see AbstractDictionary for more information
     * @param languageCode the language code of the dictionary
     * @param word the search term
     * @param max max number of results
     * @return a JSONArray with the results from the dictionary
     */
    public JSONArray searchForWord(String languageCode, String word, int max){
        return dictionaries.get(languageCode).searchForWord(word, max);
    }

    /**
     * Get the number of words that is registered with an index.
     * @return the number of words in wordIndexMap.
     */
    public long getNumberOfWords(){
        return wordIndexMap.size();
    }


    /**
     * Returns an unmodifiable collections of the dictionaries.
     *
     * @return Returns an unmodifiable collections of the dictionaries.
     */
    public Collection<AbstractDictionary> getDictionaries(){
        return Collections.unmodifiableCollection(dictionaries.values());
    }

    public JSONArray generateWord(String lang, JSONObject object, WordType type){
        return dictionaries.get(lang).generateWord(object, type);
    }
}
